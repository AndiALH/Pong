using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour
{
    // Rigidbody 2D bola
    private Rigidbody2D rigidBody2D;

    // Besarnya gaya awal yang diberikan untuk mendorong bola
    public float xInitialForce;
    public float yInitialForce;

    // Titik asal lintasan bola saat ini
    private Vector2 trajectoryOrigin;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        trajectoryOrigin = transform.position;

        // Ditambahkan delay 3 detik lagi karena saat dimainkan build nya, bola sudah meluncur saat player baru masuk sehingga player
        // tidak akan siap.
        StartCoroutine(BallDelay());

        IEnumerator BallDelay()
        {
            yield return new WaitForSeconds(3);

            // Mulai game
            RestartGame();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void ResetBall()
    {
        // Reset posisi menjadi (0,0)
        transform.position = Vector2.zero;

        // Reset kecepatan menjadi (0,0)
        rigidBody2D.velocity = Vector2.zero;
    }
    void PushBall()
    {
        // PENJELASAN MODIFIKASI TUGAS
        // Untuk modifikasi, saya akan mengubah nilai x sesuai dengan nilai random y yang didapatkan agar mendapatkan besar gaya yang sama
        // Asumsi: besar gaya yang dimaksud adalah F = root((Fx)^2 + (Fy)^2), namun karena mengimplementasikan x dan y random dengan nilai
        // F total yang selalu sama agak sulit, saya memutuskan untuk mengambil hanya 2 nilai y, yaitu 15 dan -15. Jadi total arah bola
        // random ada 4, yaitu (x, y) = {(50,15), (50,-15), (-50,15), (-50,-15)}. Untuk itu, saya akan random 4 integer dan menggunakan
        // switch untuk masuk salah satu arah x, y sesuai angka random 1-4.
        //
        // NOTES: ada hal yang ditambahkan lagi, yaitu fungsi OnCollisionEnter2D yang ada di bagian bawah script. Karena fungsi tersebut,
        // kecepatan bola akan berubah saat mengenai raket, namun kecepatan bola awal akan tetap selalu sama sehingga tetap memenuhi
        // perintah pada tugas.

        // Tentukan nilai acak antara 1 sampai 4
        float randomDirection = Random.Range(1, 5);

        // Switch untuk menentukan arah bola
        switch (randomDirection)
        {
            // Jika nilainya di bawah 1, bola bergerak ke kanan atas.
            case 1:
                rigidBody2D.AddForce(new Vector2(xInitialForce, yInitialForce));
                break;
            // Jika nilainya di bawah 1, bola bergerak ke kanan bawah.
            case 2:
                rigidBody2D.AddForce(new Vector2(xInitialForce, -yInitialForce));
                break;
            // Jika nilainya di bawah 1, bola bergerak ke kiri atas.
            case 3:
                rigidBody2D.AddForce(new Vector2(-xInitialForce, yInitialForce));
                break;
            // Jika nilainya di bawah 1, bola bergerak ke kiri bawah.
            case 4:
                rigidBody2D.AddForce(new Vector2(-xInitialForce, -yInitialForce));
                break;
        }


        //KODE LAMA DARI TUTORIAL YANG DIGANTI
        // Tentukan nilai komponen y dari gaya dorong antara -yInitialForce dan yInitialForce
        // float yRandomInitialForce = Random.Range(-yInitialForce, yInitialForce);

        // Tentukan nilai acak antara 0 (inklusif) dan 2 (eksklusif)
        //float randomDirection = Random.Range(0, 2);

        // Jika nilainya di bawah 1, bola bergerak ke kiri. 
        // Jika tidak, bola bergerak ke kanan.
        //if (randomDirection < 1.0f)
        //{
        // Gunakan gaya untuk menggerakkan bola ini.
        //    rigidBody2D.AddForce(new Vector2(-xInitialForce, yRandomInitialForce));
        //}
        //else
        //{
        //   rigidBody2D.AddForce(new Vector2(xInitialForce, yRandomInitialForce));
        //}
    }
    void RestartGame()
    {
        // Kembalikan bola ke posisi semula
        ResetBall();

        // Hanya untuk debug
        //Debug.Log("seorang player mencetak skor");

        // Setelah 2 detik, berikan gaya ke bola
        Invoke("PushBall", 2);
    }

    // Ketika bola beranjak dari sebuah tumbukan, rekam titik tumbukan tersebut
    private void OnCollisionExit2D(Collision2D collision)
    {
        trajectoryOrigin = transform.position;
    }

    // Untuk mengakses informasi titik asal lintasan
    public Vector2 TrajectoryOrigin
    {
        get { return trajectoryOrigin; }
    }

    // Fungsi ini saya tambahkan karena kalau tidak, bola akan selalu memiliki kecepatan yang sama dan trajektori yang berulang-ulang.
    // Gunanya fungsi ini adalah agar saat bola bertabrakan dengan raket, maka velocity bola akan disesuaikan dengan kecepatan bola dan
    // juga kecepatan raket.
    // Source: https://www.awesomeinc.org/tutorials/unity-pong/
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.collider.CompareTag("Player"))
        {
            Vector2 vel;
            vel.x = rigidBody2D.velocity.x;
            vel.y = (rigidBody2D.velocity.y / 2) + (coll.collider.attachedRigidbody.velocity.y / 3);
            rigidBody2D.velocity = vel;
        }
    }
}
