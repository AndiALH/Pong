# Chapter 1 - Pong

Tugas Chapter 1 dari studi independen Agate Academy bidang game programming. Hal yang dibuat kali ini adalah game Pong.

### Hal-hal yang dimodifikasi / ditambahkan

- Mengubah fungsi PushBall() pada BallControl agar kecepatan bola awal konsisten.
- Menambahkan fungsi OnCollisionEnter2D pada BallControl agar kecepatan dan arah bola dapat berubah saat mengenai raket.
- Mengubah speed dari player agar game lebih fast paced
- Menambahkan delay 3 detik sebelum bola diluncurkan pada awal game karena terkadang saat di tes di build bola sudah meluncur saat player baru masuk kedalam game.

Notes: gitignore yang digunakan sesuai dengan yang diberikan di tutorial, namun di edit sehingga file Build ikut di push ke repository git sesuai perintah dari tugas. Jika ada kesalahan seperti ada hal yang harusnya dipush namun ter ignore, mohon diberitahukan. Terima kasih.
